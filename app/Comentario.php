<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
  protected $fillable = [
      'userId','userName', 'comment', 'movie', 'created_at',
        'updated_at','rate',
  ];
}
