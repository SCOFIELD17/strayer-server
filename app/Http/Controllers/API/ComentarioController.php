<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Comentario;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ComentarioController extends Controller
{

  public function index($id){
    $data = DB::table('comentarios')->orderBy('id', 'desc')->where('movie', $id)->take(20)->get();
    foreach($data as $comentario){
      if(date("Y:m:d", strtotime($comentario->created_at)) == date('Y:m:d')) {
        $comentario->created_at = date("H:i:s", strtotime($comentario->created_at));
      }
    }
    return response()->json($data);
  }
  public function send(Request $request){
    if($request->type == 'fb'){
      $userId = 'http://graph.facebook.com/v2.5/' .$request->userId. '/picture';
      $this->validate($request, [
          'userId' => 'required|max:255',
          'comentario' => 'required|max:200|min:20',
          'movie' => 'required|max:30', //|exists:movies,id
          'userName' => 'required|max:255',
          'type' => 'required|max:2',
          'avaliacao' => 'required|numeric|min:1',
      ]);
    }else if($request->type == 'df'){
      $this->validate($request, [
          'userId' => 'required|max:255',
          'comentario' => 'required|max:200|min:20',
          'movie' => 'required|max:30', //|exists:movies,id
          'userName' => 'required|max:255',
          'type' => 'required|max:2',
          'avaliacao' => 'required|numeric|min:1',
      ]);
      $userId = $request->userId;
    }

    $data = Comentario::create(['userId' => $userId,
    'comment' => $request->comentario, 'movie' => $request->movie,
    'userName' => $request->userName, 'rate' => $request->avaliacao]);

    return response()->json($data);
  }

  public function picture($id){
    $data = DB::table('users')->where('id', $id)->get();
    return redirect($data[0]->picture);
  }

  public function noPicture($first){
    return $first;
  }
}
