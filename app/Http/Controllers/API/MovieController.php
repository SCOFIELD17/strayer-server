<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Movie;
use DB;
use Route;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;


class MovieController extends Controller
{
  private $movie;
  public function __construct(Movie $movie)
  {
    $this->movie = $movie;
  }

  public function index($amount){
    $data = $this->movie->select('id','name', 'poster','type','year')->orderBy('year','desc')->take($amount)->get();
    return $data;
  }

  public function movie($id){
    $data = DB::table('movies')->where('id', $id)->first();
    $rates = DB::table('comentarios')->select('rate','movie')->where('movie',$id)->get();

    $ratings = 0;
    $ratingM = 0;
    foreach($rates as $rate){
      $ratings = $rate->rate + $ratings;
      $ratingM = $ratingM + 1;
    }
    $ratings = $ratings / 5;
    return Response::json(array('movie' => $data,'rating' => $ratings,'ratings' => $ratingM));
  }

  public function search(Request $request){//
    if($request->searchText != ''){
      $data = $this->movie->where('name', 'LIKE', '%'.$request->searchText.'%')->orWhere('type','LIKE','%'.$request->searchText.'%')->get();
      return $data;
    }else{
      $data = $this->movie->select('id','name', 'poster','type','year')->orderBy('year','desc')->take(11)->get();
      return $data;
    }
  }

  public function addMovie(Request $request){
    $this->validate($request, [
        'name' => 'required|unique:movies,name',
        'player' => 'required|unique:movies,player',
        'description' => 'required',
    ]);

    $imageName = strtolower(preg_replace('/[^A-Za-z0-9\-]/','', str_replace(' ', '-', $request->name))).'.jpg';

    file_put_contents("public/capas/".$imageName, fopen($request->poster, 'r'));

    $description = strip_tags($request->description);
    $description = str_replace('style="text-align: center;">','', $description);
    $description = str_replace('&nbsp;','',$description);

    $type = strip_tags($request->type);
    $type = str_replace('style="text-align: center;">Gênero: ','',$type);

    $age = strip_tags($request->age);
    $age = str_replace('style="text-align: center;">Classificação etária: ','',$age);

    $time = strip_tags($request->time);
    $time = str_replace('style="text-align: center;">Tempo de Duração: ','', $time);

    $year = strip_tags($request->year);
    $year = str_replace('style="text-align: center;">Ano de Lançamento: ','', $year);

    $direction = strip_tags($request->direction);
    $direction = str_replace('style="text-align: center;">Direção: ','', $direction);

    $player = strip_tags($request->player);
    $player = str_replace('http://megaboxdownloads.com/player/player.php?id=','',$player);
    $player = $player;

    return Movie::create([
        'name' => $request->name,
        'poster' => $imageName,
        'type' => $type,
        'description' => $description,
        'time' => $time,
        'age' => $age,
        'year' => $year,
        'direction' => $direction,
        'player' => $player,
    ]);
  }

}
