<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Serie;
use App\Comentario;
use App\Episode;
use DB;
use Route;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;

class SeriesController extends Controller
{
  private $serie;
  public function __construct(Serie $serie,Episode $episode,Comentario $comentario)
  {
    $this->serie = $serie;
    $this->episode = $episode;
    $this->comentario = $comentario;
  }

  public function index($amount){
    $dates = $this->serie->select('id','name', 'poster')->take($amount)->get();
    return $dates;
  }

  public function serie($id){
    $serie = $this->serie->where('id', $id)->first();
    $rates = $this->comentario->select('rate','movie')->where('movie',$id)->get();

    $ratings = 0;
    $ratingM = 0;
    foreach($rates as $rate){
      $ratings = $rate->rate + $ratings;
      $ratingM = $ratingM + 1;
    }
    $ratings = $ratings / 5;

    $episodes = $this->episode->where('serie', $id)->get();

    return Response::json(array('serie' => $serie,'rating' => $ratings,'ratings' => $ratingM,'episodes' => $episodes));
  }
}
