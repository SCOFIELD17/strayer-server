<?php

namespace App\Http\Controllers\API;
use Hash;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use App\User;

class SettingController extends Controller
{
    public function sendMail(){

      $to = "lucasribeiroantunes@gmail.com";
      $subject = "My subject";
      $txt = "Hello world!";
      $headers = "From: lucasribeiroantunes@gmail.com" . "\r\n" .
      "CC: lucasribeiroantunes@gmail.com";

      mail($to,$subject,$txt,$headers);

      return 'louco';
    }

    public function picture(Request $request){
      $this->validate($request, [
          'picture' => 'required|url',
          'email' => 'required|email|max:150',
      ]);

      User::where('email', $request->email)
          ->update(['picture' => $request->picture]);
      return ['success' => 'Foto alterada com sucesso.'];
    }

    public function password(Request $request){

      $this->validate($request, [
          'password_now' => 'required|max:100',
          'password_new' => 'required|max:100|confirmed',
          'email' => 'required|email|max:150',
      ]);

      if (Auth::check() or Auth::attempt(['email' => $request->email, 'password' => $request->password_now])){

        $user = Auth::user();
        $user->password = Hash::make($request->password_new);
        $user->save();
        return ['success' => 'Senha alterada com sucesso.'];

      }else{

        return Response::json(['message' => 'Credenciais informadas não correspondem com nossos registros.'], 500);

      }

    }
}
