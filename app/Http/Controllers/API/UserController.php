<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
  public function Login(Request $request)
  {
      $this->validate($request, [
          'email' => 'required|max:255|email',
          'password' => 'required|max:255|',
      ]);
      if (Auth::check() or Auth::attempt(['email' => $request->email, 'password' => $request->password])){
        return ['message' => 'success' , 'id' => Auth::user()->id, 'name' => Auth::user()->name];
      }else{
        return ['message' => 'error'];
      }
  }

  public function Register(Request $request){
    $this->validate($request, [
        'nome' => 'required|max:50',
        'email' => 'required|max:100|email|unique:users,email',
        'senha' => 'required|max:100|confirmed',
    ]);
    return User::create([
        'name' => $request->nome,
        'email' => $request->email,
        'password' => bcrypt($request->senha),
    ]);
  }
}
