<?php

namespace App\Http\Controllers;

use Redirect;
use Illuminate\Http\Request;
use App\Serie;
use App\Episode;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class SerieController extends Controller
{

    public function clean($string) {
       $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

       return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function Create(Request $request){

      $name = $request->name;
      $poster = $this->clean(strtolower($request->name)).'.jpg';
      $type = $request->type;
      $description = $request->description;

      $image = Input::file('poster');
      $image->move(public_path().'/capas/', $poster);

      Serie::create(array('name' => $name, 'poster' => $poster, 'type' => $type, 'description' => $description));
      return Redirect::back();

    }

    public function episode(){
      $series = Serie::select('id','name')->get();
      return view('addep')->with('series'  , $series);
    }

    public function createEpisode(Request $request){
      $serie = $request->serie;
      $season = $request->season;
      $number = $request->number;
      $player = $request->player;
      $type = $request->type;

      Episode::create(['serie' => $serie, 'season' => $season, 'number' => $number, 'player' => $player, 'type' => $type]);
      return Redirect::back();
    }
}
