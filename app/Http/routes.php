<?php
// WebSite
Route::get('/contato', 'Web\ContactController@index');

Route::get('/', function () {
    return view('home');
});

Route::get('/serie/adicionar', function () {
    return view('addserie');
});
Route::get('/serie/episodio/adicionar', 'SerieController@episode');
Route::post('/serie/episodio/adicionar', 'SerieController@createEpisode');

Route::post('/serie/adicionar','SerieController@Create');
// End WebSite

// Json API
Route::post('/user/login', 'API\UserController@Login');
Route::post('/user/register', 'API\UserController@Register');

Route::post('/search', 'API\MovieController@search');
Route::get('/movies/preview/{id}', 'API\MovieController@movie');
Route::post('/movies/add', 'API\MovieController@addMovie');

Route::get('/movies/{amount}', 'API\MovieController@index');
Route::get('/comentario/{id}', 'API\ComentarioController@index');
Route::post('/comentario/send', 'API\ComentarioController@send');
Route::get('/comentario/picture/{id}', 'API\ComentarioController@picture');
Route::get('/comentario/nopicture/{first}','API\ComentarioController@noPicture');
Route::get('teste','API\MovieController@teste');

Route::get('/setting/about','API\SettingController@sendMail');

Route::post('/setting/password', 'API\SettingController@password');
Route::post('/setting/picture', 'API\SettingController@picture');

Route::get('/series/{amount}','API\SeriesController@index');
Route::get('/series/preview/{id}','API\SeriesController@serie');
// End Json API
//
// Route::group(['middleware' => 'web'], function () {
//     Route::auth();
//     Route::get('/home', 'HomeController@index');
//     Route::get('/mensagens','painel\MessagesController@index');
// });
