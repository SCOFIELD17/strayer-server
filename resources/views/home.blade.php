@extends('layouts.app')
@section('content')
<section id="header-bg">
  <div class="header">
    <div class="container">
      <div class="row">
        <div class="col s12 m6">
          <h1>Let's Go Bitch</h1>
          <h5>
            Strayer é um aplicativo que ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </h5>
        </div>
        <div class="col s12 m6">
          <img src="{{asset('public/images/gadget.png')}}" alt="" />
        </div>
      </div>
    </div>
  </div>
</section>

<section id="" class="center-align">
  <h2>Como funciona</h2>
  <div class="container">
    <div class="row">
      <div class="col s12 m4">
        <img src="{{asset('public/images/icon-movie.png')}}" alt="" />
        <h4>Gratuito</h4>
        <p>
          Assista os melhores filmes gratuitamente, direto do seu Smartphone.
        </p>
      </div>
      <div class="col s12 m4">
        <img src="{{asset('public/images/icon-rating.png')}}" alt="" />
        <h4>Avalie</h4>
        <p>
          Deixe seu feedback sobre os filmes, veja a opnião de outras pessoas.
        </p>
      </div>
      <div class="col s12 m4">
        <img src="{{asset('public/images/icon-library.png')}}" width="134px"alt="" />
        <h4>Diversidade</h4>
        <p>
         Vasta bibliotéca de filmes e gêneros
        </p>
      </div>
    </div>
  </div>

</section>
@endsection
