<?php $__env->startSection('content'); ?>
<div class="container spark-screen">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Messages</div>

                <div class="panel-body">
                  <table class="table table-condensed">
                    <th>#</th><th>Nome</th><th>Email</th><th>Comentário</th><th>Ação</th>
                    <?php foreach($messages as $message): ?>
                      <tr><td><?php echo e($message->id); ?></td><td><?php echo e($message->name); ?></td><td><?php echo e($message->mail); ?></td><td><?php echo e(str_limit($message->text, $limit = 50, $end = '...')); ?></td>
                      <td>
                        <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></button>
                        <button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                      </td></tr>
                    <?php endforeach; ?>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>