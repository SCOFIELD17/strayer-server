<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Adicionar Episódio</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

  </head>
  <body>
    <div class="container">
      <div class="page-header">
        <h1>Adicione novos Episódios</h1>
      </div>
      <div class="row">
        <div class="col col-md-6">
          <form method="POST" action="adicionar">
            <div class="form-group">
              <div class="form-group">
                <label for="id">Série</label>
                <select class="form-control" name="serie">
                  <?php foreach($series as $serie): ?>
                    <option value="<?php echo e($serie->id); ?>" selected><?php echo e($serie->name); ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="season">Temporada</label>
              <input type="text" class="form-control" name="season" placeholder="1 Temporada">
            </div>
            <div class="form-group">
              <label for="number">Número</label>
              <input type="text" class="form-control" name="number" placeholder="1 EP">
            </div>
            <div class="form-group">
              <label for="player">Player</label>
              <input type="text" class="form-control" name="player" placeholder="http://docs.google.com">
            </div>
            <div class="form-group">
              <label for="id">Tipo</label>
              <select class="form-control" name="type">
                  <option value="0" selected>Legendado</option>
                  <option value="1">Dublado</option>
              </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>
