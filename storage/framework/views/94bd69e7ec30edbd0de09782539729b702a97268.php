<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Adicionar série</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

  </head>
  <body>
    <div class="container">
      <div class="page-header">
        <h1>Adicione novas séries</h1>
      </div>
      <div class="row">
        <div class="col col-md-6">
          <form method="POST" action="adicionar" enctype="multipart/form-data">
            <div class="form-group">
              <label for="name">Nome da série</label>
              <input type="text" class="form-control" name="name" placeholder="Breaking bad">
            </div>
            <div class="form-group">
              <label for="name">Gênero</label>
              <input type="text" class="form-control" name="type" placeholder="Terror, Comédia">
            </div>
            <div class="form-group">
              <label for="poster">Poster</label>
              <input type="file" name="poster">
              <p class="help-block">Selecione o poster</p>
            </div>
            <div class="form-group">
              <label for="name">Descrição</label>
              <textarea rows="8" cols="40" name="description" class="form-control"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>
