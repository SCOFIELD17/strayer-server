<?php $__env->startSection('content'); ?>
<section id="header-bg">
  <div class="header">
    <div class="container">
      <div class="row">
        <div class="col s12 m6">
          <h1>Contato</h1>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="contato" class="center-align">
  <div class="container">
    <div class="row">
      <form class="col s12">
           <div class="row">
             <div class="input-field col s6">
               <input id="last_name" type="text" class="validate">
               <label for="last_name">Nome</label>
             </div>
             <div class="input-field col s6">
               <input id="last_name" type="text" class="validate">
               <label for="last_name">Sobrenome</label>
             </div>
           </div>
           <div class="row">
             <div class="input-field col s12">
               <input id="email" type="email" class="validate">
               <label for="email" data-error="Inválido.">Email</label>
             </div>
           </div>
           <div class="row">
             <div class="input-field col s12">
               <input id="subject" type="text" class="validate">
               <label for="subject" data-error="Inválido.">Assunto</label>
             </div>
           </div>
           <div class="row">
            <div class="input-field col s12">
              <textarea id="message" class="materialize-textarea"></textarea>
              <label for="message">Mensagem</label>
            </div>
          </div>
         </form>
    </div>
  </div>

  <button class="btn waves-effect waves-light" type="submit" name="action">Enviar
    <i class="material-icons right">send</i>
  </button>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>