<!DOCTYPE html>
<html manifest="cache.appcache">
  <head>
    <title>Strayer - Aplicativo de filmes</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
  </head>

  <body>
    <nav>
      <div class="nav-wrapper">
        <a href="#" class="brand-logo"><img class="logo" src="<?php echo e(asset('public/images/logo.png')); ?>" alt="Logo" /></a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
          <li><a href="<?php echo e(url('/')); ?>">Início</a></li>
          <li><a href="#">Baixar Agora</a></li>
          <li><a href="<?php echo e(url('/contato')); ?>">Contate-nos</a></li>
        </ul>
        <ul id="slide-out" class="side-nav">
          <li><a href="#">Início</a></li>
          <li><a href="#">Baixar Agora</a></li>
          <li><a href="contato.html">Contate-nos</a></li>
        </ul>
        <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
      </div>
    </nav>

    <?php echo $__env->yieldContent('content'); ?>

            <footer class="page-footer">
              <div class="row">
                <div class="col s12">
                  <div class="row">
                    <div class="col l6 s12">
                      <img width="140" src="<?php echo e(asset('public/images/logo.png')); ?>" alt="Logo" />
                      <p class="grey-text text-lighten-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                    <div class="col l4 offset-l2 s12">
                      <h5 class="white-text">Links</h5>
                      <ul>
                        <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="footer-copyright">
                <div class="row">
                  <div class="col s12">
                    © 2016, Strayer
                    <a class="grey-text text-lighten-4 right" href="#!">Made with ♥ by SCOFIELD</a>
                  </div>
                </div>
              </div>
            </footer>

    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('public/css/materialize.min.css')); ?>"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('public/css/custom.css')); ?>"  media="screen,projection"/>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="<?php echo e(asset('public/js/materialize.min.js')); ?>"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        $('.parallax').parallax();
        $(".button-collapse").sideNav();
      });
    </script>

    <script type="text/javascript">
      var _urq = _urq || [];
      _urq.push(['initSite', 'c956baa0-519c-4d81-9d52-ab41056d2100']);
      (function() {
      var ur = document.createElement('script'); ur.type = 'text/javascript'; ur.async = true;
      ur.src = ('https:' == document.location.protocol ? 'https://cdn.userreport.com/userreport.js' : 'http://cdn.userreport.com/userreport.js');
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ur, s);
      })();
    </script>
  </body>
</html>
